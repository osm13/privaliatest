package oriolseguramorales.privaliatest.features.splash.ui.view

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.support.v7.app.AppCompatActivity
import oriolseguramorales.privaliatest.R
import oriolseguramorales.privaliatest.features.movies.ui.view.activitiy.MoviesListActivity

class SplashActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        val handler = Handler()
        handler.postDelayed({
            startActivity(MoviesListActivity.callingIntent(this))
            finish()
        }, 3000)
    }
}
