package oriolseguramorales.privaliatest.features.movies.ui.view.adapter

import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.squareup.picasso.Callback
import com.squareup.picasso.Picasso
import oriolseguramorales.privaliatest.R
import oriolseguramorales.privaliatest.features.movies.domain.model.Movie
import java.io.InterruptedIOException
import java.text.SimpleDateFormat
import java.util.*


class MoviesAdapter(var movies:ArrayList<Movie> = ArrayList<Movie>() ): RecyclerView.Adapter<MoviesAdapter.ViewHolder>() {

    class ViewHolder(view: View?) : RecyclerView.ViewHolder(view) {
        private var title:TextView = itemView.findViewById(R.id.card_title)
        private var overview:TextView = itemView.findViewById(R.id.card_overview)
        private var year:TextView = itemView.findViewById(R.id.card_year)
        private var image:ImageView = itemView.findViewById(R.id.card_imageView)
        val formatter = SimpleDateFormat("YYYY-MM-dd", Locale.ENGLISH)
        val yearFormatter = SimpleDateFormat("yyyy", Locale.ENGLISH)


        fun bindMovie(movie: Movie) {

            try {
                Picasso.get().load("https://image.tmdb.org/t/p/w500/" + movie.poster_path)
                    .into(image, object : Callback {
                        override fun onSuccess() {
                            title.text = movie.title
                            overview.text = movie.overview
                            if(movie.release_date != null){
                                val date = formatter.parse(movie.release_date)
                                year.text = yearFormatter.format(date)
                            }
                        }

                        override fun onError(e: Exception?) {
                            Log.w("MoviesAdapter", "Load image error")
                        }
                    })
            } catch (e: InterruptedIOException){
                Log.w("SearchMoviesUseCaseImpl", "Work has been interrupted")
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.movie_card, parent, false)

        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return movies.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindMovie(movies[position])
    }




}