package oriolseguramorales.privaliatest.features.movies.data.repository.impl

import oriolseguramorales.privaliatest.core.api.Retrofit
import oriolseguramorales.privaliatest.features.movies.domain.model.Movie
import oriolseguramorales.privaliatest.features.movies.data.repository.MoviesRepository
import java.io.InterruptedIOException

class MoviesRepositoryImpl(val retrofit: Retrofit):MoviesRepository {

    override fun getMovieList(
        pageNumber: Int,
        onMoviesListReceivedOK: (movies: List<Movie>) -> Unit,
        onMoviesListReceivedError: () -> Unit
    ) {
        val movies = retrofit.listResponse(pageNumber).execute()
        if(movies.isSuccessful){
            onMoviesListReceivedOK(movies.body()?.results ?: emptyList())
        } else {
            onMoviesListReceivedError()
        }
    }

    @Throws(InterruptedIOException::class)
    override fun searchMovies (
        pageNumber: Int,
        query: String,
        onMoviesListReceivedOK: (movies: List<Movie>) -> Unit,
        onMoviesListReceivedError: () -> Unit
    ) {
        val movies = retrofit.searchMovie(hashMapOf("page" to pageNumber.toString(), "query" to query)).execute()
        if(movies.isSuccessful){
            onMoviesListReceivedOK(movies.body()?.results ?: emptyList())
        } else {
            onMoviesListReceivedError()
        }
    }
}