package oriolseguramorales.privaliatest.features.movies.domain.usecase

import oriolseguramorales.privaliatest.features.movies.domain.model.Movie

interface GetMoviesListUseCase {

    fun getMovieList(
        pageNumber:Int,
        onMoviesListReceivedOK: (movies:List<Movie>) -> Unit, onMoviesListReceivedError: () -> Unit
    )

}