package oriolseguramorales.privaliatest.features.movies.ui.presenter

import oriolseguramorales.privaliatest.features.movies.domain.model.Movie

interface SearchMoviesPresenter {

    fun searchMovies(
        pageNumber:Int,
        query:String,
        onMoviesListReceivedOK: (movies:List<Movie>) -> Unit, onMoviesListReceivedError: () -> Unit
    )

    interface View{
        fun onMoviesListReceivedOK(movies:List<Movie>)
        fun onMoviesListReceivedError()
    }
}