package oriolseguramorales.privaliatest.features.movies.ui.presenter.impl

import oriolseguramorales.privaliatest.features.movies.domain.model.Movie
import oriolseguramorales.privaliatest.features.movies.domain.usecase.GetMoviesListUseCase
import oriolseguramorales.privaliatest.features.movies.domain.usecase.SearchMoviesUseCase
import oriolseguramorales.privaliatest.features.movies.ui.presenter.MoviesListPresenter

class MoviesListPresenterImpl(private val getMoviesListUseCase: GetMoviesListUseCase) : MoviesListPresenter {

    var view:MoviesListPresenter.View? = null

    override fun getMovieList(
        pageNumber: Int,
        onMoviesListReceivedOK: (movies:List<Movie>) -> Unit,
        onMoviesListReceivedError: () -> Unit
    ) {
        getMoviesListUseCase.getMovieList(pageNumber,onMoviesListReceivedOK,onMoviesListReceivedError)
    }

}