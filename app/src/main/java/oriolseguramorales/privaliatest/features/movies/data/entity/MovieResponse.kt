package oriolseguramorales.privaliatest.features.movies.data.entity

import oriolseguramorales.privaliatest.features.movies.domain.model.Movie
import java.io.Serializable

data class MovieResponse(var page:Int, var total_pages:Int, var results:List<Movie>):Serializable