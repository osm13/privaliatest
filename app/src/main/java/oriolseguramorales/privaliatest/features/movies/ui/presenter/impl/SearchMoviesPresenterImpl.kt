package oriolseguramorales.privaliatest.features.movies.ui.presenter.impl

import oriolseguramorales.privaliatest.features.movies.domain.model.Movie
import oriolseguramorales.privaliatest.features.movies.domain.usecase.SearchMoviesUseCase
import oriolseguramorales.privaliatest.features.movies.ui.presenter.SearchMoviesPresenter

class SearchMoviesPresenterImpl(private val searchMoviesUseCase: SearchMoviesUseCase): SearchMoviesPresenter {
    override fun searchMovies(
        pageNumber: Int,
        query: String,
        onMoviesListReceivedOK: (movies: List<Movie>) -> Unit,
        onMoviesListReceivedError: () -> Unit
    ) {
        searchMoviesUseCase.searchMovies(pageNumber, query, onMoviesListReceivedOK, onMoviesListReceivedError)
    }
}