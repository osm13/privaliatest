package oriolseguramorales.privaliatest.features.movies.domain.model

data class Movie(
    var id:Int,
    var title:String,
    var poster_path:String,
    var overview:String,
    var release_date:String

)