package oriolseguramorales.privaliatest.features.movies.ui.presenter

import oriolseguramorales.privaliatest.features.movies.domain.model.Movie

interface MoviesListPresenter {
    fun getMovieList(
        pageNumber:Int,
        onMoviesListReceivedOK: (movies:List<Movie>) -> Unit, onMoviesListReceivedError: () -> Unit
    )


    interface View{
        fun onMoviesListReceivedOK(movies:List<Movie>)
        fun onMoviesListReceivedError()
    }
}