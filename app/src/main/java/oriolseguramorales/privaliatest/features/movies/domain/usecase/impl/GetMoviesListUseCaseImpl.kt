package oriolseguramorales.privaliatest.features.movies.domain.usecase.impl

import oriolseguramorales.privaliatest.features.movies.data.repository.MoviesRepository
import oriolseguramorales.privaliatest.features.movies.domain.model.Movie
import oriolseguramorales.privaliatest.features.movies.domain.usecase.GetMoviesListUseCase

class GetMoviesListUseCaseImpl(val moviesRepository: MoviesRepository): GetMoviesListUseCase {

    override fun getMovieList(
        pageNumber: Int,
        onMoviesListReceivedOK: (movies: List<Movie>) -> Unit,
        onMoviesListReceivedError: () -> Unit
    ) {
        Thread(Runnable {
            moviesRepository.getMovieList(pageNumber, onMoviesListReceivedOK, onMoviesListReceivedError)
        }).start()
    }
}