package oriolseguramorales.privaliatest.features.movies.domain.usecase

import oriolseguramorales.privaliatest.features.movies.domain.model.Movie

interface SearchMoviesUseCase {
    fun searchMovies(
        pageNumber:Int,
        query:String,
        onMoviesListReceivedOK: (movies:List<Movie>) -> Unit, onMoviesListReceivedError: () -> Unit
    )
}