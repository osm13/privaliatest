package oriolseguramorales.privaliatest.features.movies.domain.usecase.impl

import android.util.Log
import oriolseguramorales.privaliatest.features.movies.data.repository.MoviesRepository
import oriolseguramorales.privaliatest.features.movies.domain.model.Movie
import oriolseguramorales.privaliatest.features.movies.domain.usecase.SearchMoviesUseCase
import java.io.InterruptedIOException

class SearchMoviesUseCaseImpl(val moviesRepository: MoviesRepository) : SearchMoviesUseCase {

    var workList: ArrayList<Thread> = ArrayList()

    override fun searchMovies(
        pageNumber: Int,
        query: String,
        onMoviesListReceivedOK: (movies: List<Movie>) -> Unit,
        onMoviesListReceivedError: () -> Unit
    ) {
        try {

            val thread = Thread(Runnable {
                moviesRepository.searchMovies(pageNumber, query, onMoviesListReceivedOK, onMoviesListReceivedError)
            })
            thread.start()
        } catch (e: InterruptedIOException){
            Log.w("SearchMoviesUseCaseImpl", "Work has been interrupted")
        }
    }
}