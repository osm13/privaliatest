package oriolseguramorales.privaliatest.features.movies.ui.view.activitiy

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.Menu
import android.widget.Toast
import oriolseguramorales.privaliatest.R
import oriolseguramorales.privaliatest.core.PrivaliaTestApplication
import oriolseguramorales.privaliatest.features.movies.domain.model.Movie
import oriolseguramorales.privaliatest.features.movies.ui.presenter.MoviesListPresenter
import oriolseguramorales.privaliatest.features.movies.ui.view.adapter.MoviesAdapter
import oriolseguramorales.privaliatest.features.movies.ui.view.listener.InfiniteScrollListener
import javax.inject.Inject


class MoviesListActivity : AppCompatActivity(), MoviesListPresenter.View {

    var moviesAdapter: MoviesAdapter = MoviesAdapter()
    var pageNumber = 0

    @Inject
    lateinit var presenter: MoviesListPresenter

    companion object {
        fun callingIntent(context: Context) = Intent(context, MoviesListActivity::class.java)
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_movies_list)

        PrivaliaTestApplication.daggerAppComponent.inject(this)
        val linearLayoutManager = LinearLayoutManager(this)

        val recyclerView :RecyclerView = findViewById(R.id.movies_list_recyclerview)
        recyclerView.adapter = moviesAdapter
        recyclerView.layoutManager = linearLayoutManager

        val getList = {
            pageNumber++

            presenter.getMovieList(pageNumber, {
                runOnUiThread {
                    moviesAdapter.movies.addAll(it)
                    moviesAdapter.notifyDataSetChanged()
                }
            },
                {
                    (Toast.makeText(this, "Error al cargar los datos", Toast.LENGTH_SHORT).show())
                })
        }
        getList()
        recyclerView.addOnScrollListener(InfiniteScrollListener(getList, linearLayoutManager))
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.list_menu, menu)

        val searchItem = menu?.findItem(R.id.searchIcon)
        searchItem?.setOnMenuItemClickListener {
            startActivity(SearchMoviesActivity.callingIntent(this))
            true
        }

        return super.onCreateOptionsMenu(menu)
    }


    override fun onMoviesListReceivedOK(movies:List<Movie>) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onMoviesListReceivedError() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}
