package oriolseguramorales.privaliatest.features.movies.data.repository

import oriolseguramorales.privaliatest.features.movies.domain.model.Movie

interface MoviesRepository {
    fun getMovieList(
        pageNumber:Int,
        onMoviesListReceivedOK: (movies:List<Movie>) -> Unit, onMoviesListReceivedError: () -> Unit
    )

    fun searchMovies(
        pageNumber:Int,
        query:String,
        onMoviesListReceivedOK: (movies:List<Movie>) -> Unit, onMoviesListReceivedError: () -> Unit
    )

}