package oriolseguramorales.privaliatest.features.movies.ui.view.activitiy

import android.content.Context
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.SearchView
import android.view.Menu
import android.widget.Toast
import oriolseguramorales.privaliatest.R
import oriolseguramorales.privaliatest.core.PrivaliaTestApplication
import oriolseguramorales.privaliatest.features.movies.domain.model.Movie
import oriolseguramorales.privaliatest.features.movies.ui.presenter.MoviesListPresenter
import oriolseguramorales.privaliatest.features.movies.ui.presenter.SearchMoviesPresenter
import oriolseguramorales.privaliatest.features.movies.ui.view.adapter.MoviesAdapter
import oriolseguramorales.privaliatest.features.movies.ui.view.listener.InfiniteScrollListener
import javax.inject.Inject

class SearchMoviesActivity : AppCompatActivity(), MoviesListPresenter.View, SearchView.OnQueryTextListener  {

    var moviesAdapter: MoviesAdapter = MoviesAdapter()
    var pageNumber = 0
    var searchText = ""
    private var searchView: SearchView? = null

    @Inject
    lateinit var presenter: SearchMoviesPresenter

    companion object {
        fun callingIntent(context: Context) = Intent(context, SearchMoviesActivity::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search_movies)

        PrivaliaTestApplication.daggerAppComponent.inject(this)
        val linearLayoutManager = LinearLayoutManager(this)

        val recyclerView : RecyclerView = findViewById(R.id.search_movies_recyclerview)
        recyclerView.adapter = moviesAdapter
        recyclerView.layoutManager = linearLayoutManager

        val getList = {
            pageNumber++
            presenter.searchMovies(pageNumber, searchText, {
                runOnUiThread {
                    moviesAdapter.movies.clear()
                    moviesAdapter.movies.addAll(it)
                    moviesAdapter.notifyDataSetChanged()
                }
            },
                {
                    Toast.makeText(this, "Error loading data", Toast.LENGTH_SHORT).show()
                })
        }
        recyclerView.addOnScrollListener(InfiniteScrollListener(getList, linearLayoutManager))
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.search_menu, menu)

        val searchItem = menu?.findItem(R.id.searchBar)

        searchView = searchItem?.actionView as SearchView
        searchView?.queryHint = "Search Movies"
        searchView?.setOnQueryTextListener(this)
        searchView?.onActionViewExpanded()

        return super.onCreateOptionsMenu(menu)
    }

    override fun onQueryTextSubmit(query: String?): Boolean {
        queryText(query)
        return true
    }

    override fun onQueryTextChange(newText: String?): Boolean {
        queryText(newText)
        return true
    }

    fun queryText(newText: String?) {
        if (newText != null) {
            if(newText == ""){
                moviesAdapter.movies.clear()
                moviesAdapter.notifyDataSetChanged()
            } else {
                searchText = newText
                pageNumber = 1

                presenter.searchMovies(pageNumber, newText, {
                    runOnUiThread {
                        moviesAdapter.movies.clear()
                        moviesAdapter.movies.addAll(it)
                        moviesAdapter.notifyDataSetChanged()
                    }
                },
                    {
                        Toast.makeText(this, "Error loading data", Toast.LENGTH_SHORT).show()
                    })
            }

        }
    }


    override fun onMoviesListReceivedOK(movies: List<Movie>) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onMoviesListReceivedError() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

}
