package oriolseguramorales.privaliatest.core.api

import oriolseguramorales.privaliatest.features.movies.data.entity.MovieResponse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query
import retrofit2.http.QueryMap

interface Retrofit {
    @GET("3/movie/popular?api_key=93aea0c77bc168d8bbce3918cefefa45&language=es-ES")
    fun listResponse(@Query("page") page:Int):Call<MovieResponse>


    @GET("3/search/movie?api_key=93aea0c77bc168d8bbce3918cefefa45&language=es-ES")
    fun searchMovie(@QueryMap filters: Map<String, String> ):Call<MovieResponse>


}