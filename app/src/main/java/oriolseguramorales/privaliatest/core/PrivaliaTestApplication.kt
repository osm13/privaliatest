package oriolseguramorales.privaliatest.core

import android.app.Application
import oriolseguramorales.privaliatest.core.di.AppComponent
import oriolseguramorales.privaliatest.core.di.DaggerAppComponent

class PrivaliaTestApplication : Application() {
    companion object {
        val daggerAppComponent: AppComponent = DaggerAppComponent.builder()
            .build()
    }

    override fun onCreate() {
        super.onCreate()
        daggerAppComponent.inject(this)
    }
}
