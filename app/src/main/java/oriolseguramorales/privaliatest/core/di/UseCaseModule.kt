package oriolseguramorales.privaliatest.core.di

import dagger.Module
import dagger.Provides
import oriolseguramorales.privaliatest.features.movies.data.repository.MoviesRepository
import oriolseguramorales.privaliatest.features.movies.domain.usecase.GetMoviesListUseCase
import oriolseguramorales.privaliatest.features.movies.domain.usecase.SearchMoviesUseCase
import oriolseguramorales.privaliatest.features.movies.domain.usecase.impl.GetMoviesListUseCaseImpl
import oriolseguramorales.privaliatest.features.movies.domain.usecase.impl.SearchMoviesUseCaseImpl

@Module
class UseCaseModule {
    @Provides
    fun providesGetMoviesListUseCase(moviesRepository: MoviesRepository):GetMoviesListUseCase{
        return GetMoviesListUseCaseImpl(moviesRepository)
    }

    @Provides
    fun providesSearchMoviesUseCase(moviesRepository: MoviesRepository):SearchMoviesUseCase{
        return SearchMoviesUseCaseImpl(moviesRepository)
    }
}