package oriolseguramorales.privaliatest.core.di

import dagger.Component
import oriolseguramorales.privaliatest.core.PrivaliaTestApplication
import oriolseguramorales.privaliatest.features.movies.ui.view.activitiy.MoviesListActivity
import oriolseguramorales.privaliatest.features.movies.ui.view.activitiy.SearchMoviesActivity

@Component(modules = [
    PresenterModule::class,
    UseCaseModule::class,
    RepositoryModule::class,
    APIModule::class])
interface AppComponent {
    fun inject(privaliaTestApplication: PrivaliaTestApplication)
    fun inject(moviesListActivity: MoviesListActivity)
    fun inject(searchMoviesActivity: SearchMoviesActivity)
}