package oriolseguramorales.privaliatest.core.di

import dagger.Module
import dagger.Provides
import oriolseguramorales.privaliatest.core.api.Retrofit
import oriolseguramorales.privaliatest.features.movies.data.repository.MoviesRepository
import oriolseguramorales.privaliatest.features.movies.data.repository.impl.MoviesRepositoryImpl

@Module
class RepositoryModule {
    @Provides
    fun providesMoviesRepository(retrofit: Retrofit):MoviesRepository{
        return MoviesRepositoryImpl(retrofit)
    }
}