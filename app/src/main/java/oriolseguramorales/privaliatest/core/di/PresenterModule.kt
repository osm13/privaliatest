package oriolseguramorales.privaliatest.core.di

import dagger.Module
import dagger.Provides
import oriolseguramorales.privaliatest.features.movies.domain.usecase.GetMoviesListUseCase
import oriolseguramorales.privaliatest.features.movies.domain.usecase.SearchMoviesUseCase
import oriolseguramorales.privaliatest.features.movies.ui.presenter.MoviesListPresenter
import oriolseguramorales.privaliatest.features.movies.ui.presenter.SearchMoviesPresenter
import oriolseguramorales.privaliatest.features.movies.ui.presenter.impl.MoviesListPresenterImpl
import oriolseguramorales.privaliatest.features.movies.ui.presenter.impl.SearchMoviesPresenterImpl

@Module
class PresenterModule {
    @Provides
    fun providesMoviesListPresenter(getMoviesListUseCase: GetMoviesListUseCase):MoviesListPresenter{
        return MoviesListPresenterImpl(getMoviesListUseCase)
    }

    @Provides
    fun providesSearchMoviesPresenter(searchMoviesUseCase: SearchMoviesUseCase): SearchMoviesPresenter {
        return SearchMoviesPresenterImpl(searchMoviesUseCase)
    }
}